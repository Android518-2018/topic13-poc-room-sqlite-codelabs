# Room + SQLite example

This is the code generated when you follow [codelabs tutorial room + sqlite](https://codelabs.developers.google.com/codelabs/android-room-with-a-view/]

Another examle of using Room + ViewModel + LiveData may be found here
https://github.com/googlesamples/android-architecture-components/tree/master/BasicSample